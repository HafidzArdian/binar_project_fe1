// ==================== 🔥 SOAL NO 1 🔥 ===============================
/* Buatlah sebuah function dengan nama changeWord yang berfungsi
untuk menggantikan sebuah kata didalam sebuah kalimat.

Function ini akan menerima 3 parameter, yaitu :

selectedText => Kata yang terdapat pada sebuah kalimat dan
merupakan kata yang akan diganti nantinya.

changedText => Kata yang akan menjadi pengganti pada sebuah
kalimat nantinya.

text => Sebuah kalimat. */
// changeWord = (selectedText, changedText, text) => {
//   // Memisahkan kalimat menjadi array kata-kata
//   const words = text.split(" ");

//   // Mengubah kata terpilih dengan kata yang baru
//   const newWords = words.map((word) => {
//     if (word === selectedText) {
//       return changedText;
//     }
//     return word;
//   });

//   // Menggabungkan kata-kata kembali menjadi kalimat utuh
//   const newSentence = newWords.join(" ");

//   return newSentence;
// };

// const kalimat1 = "Andini Sangat Mencintai Kamu Selamanya";
// const kalimat2 =
//   " Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu";

// // expected result
// // ketika function tersebut dipanggil dengan variabel kalimat 1
// console.log(changeWord("Mencintai", "membenci", kalimat1));
// // output yang harus keluar adalah
// // => 'Andini sangat membenci kamu selamanya '

// // ketika function tersebut dipanggil dengan variabel kalimat 2
// console.log(changeWord("bromo", "semeru", kalimat2));
// // output yang harus keluar adalah
// // => ' Gunung semeru tak akan mampu menggambarkan besarnya cintaku padamu '

/* 
  //membuat menggunakan replace
  function changeWord(selectedText,Text){
    // metode untuk mengubah string
    let change = Text.replace(selectedText, Text)
    return change
  }
  const kalimat1 = "Andini sangat mencintai kamu selamanya";
  const kalimat2 = "Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu";

  console.log(changeWord('mencintai', 'membenci', kalimat1))
  console.log(changeWord('bromo', 'semeru', kalimat2))
  
  
  
 */
// ==================== 🔥 SOAL NO 2 🔥 ===============================

// checkTypeNumber = (givenNumber) => {
//   if (typeof givenNumber == "number") {
//     if (givenNumber % 2 == 0) {
//       return "GENAP";
//     } else {
//       return "GANJIL";
//     }
//   } else if (givenNumber == null) {
//     return "Error : Bro where is the parameter?";
//   } else {
//     return "Error : Invalid data type";
//   }
// };

// console.log(checkTypeNumber(10));
// console.log(checkTypeNumber(3));
// console.log(checkTypeNumber("3"));
// console.log(checkTypeNumber({}));
// console.log(checkTypeNumber([]));
// console.log(checkTypeNumber());

/* 
checkTypeNumber = (givenNumber) => {
  if(givenNumber === undefined){

    return "Error : Bro Where is the parameter?";
  }
  
  else if (typeof givenNumber !== "number"){
    return "Error : invalid Data Type"
  }
  
  else return givenNumber % 2 === 0 ? "Genap" : "Ganjil";
};

console.log(checkTypeNumber(10));
console.log(checkTypeNumber(3));
console.log(checkTypeNumber("3"));
console.log(checkTypeNumber({}));
console.log(checkTypeNumber([]));
console.log(checkTypeNumber());

*/
// ==================== 🔥 SOAL NO 3 🔥 ===============================

// function checkEmail(email) {
//   // validasi tipe data parameter
//   if (typeof email !== "string") {
//     return "INVALID: Parameter harus berupa string";
//   }
//   // validasi format email menggunakan regular expression
//   const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
//   if (regex.test(email)) {
//     return "VALID";
//   } else {
//     return "INVALID: Format email tidak valid";
//   }
// }

// function checkTypeNumber(a) {
//   // validasi tipe data number pada string
//   return !isNaN(a)
//     ? "Hasil validasi email adalah angka"
//     : "Hasil validasi email bukan angka";
// }

// console.log(checkEmail("apranata@binar.co.id"));
// console.log(checkEmail("apranata@binar.com"));
// console.log(checkEmail("apranata@binar"));
// console.log(checkEmail("apranata"));
// console.log(checkTypeNumber(checkEmail("3322")));
// console.log(checkEmail());

// ==================== 🔥 SOAL NO 4 🔥 ===============================
// function isValidPassword(password) {
//   if (!password) {
//     console.error("Error: Password is required");
//     return false;
//   }
//   if (typeof password !== "string") {
//     console.error("Error: Password must be a string");
//     return false;
//   }
//   const isValid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[\w\W]{8,}$/.test(password);
//   return isValid;
// }
// console.log(isValidPassword("Meong@2021"));
// console.log(isValidPassword("@eong"));
// console.log(isValidPassword("Meong2"));
// console.log(isValidPassword(0));
// console.log(isValidPassword());

// ==================== 🔥 SOAL NO 5 🔥 ===============================
// function getSplitName(personName) {
//   if (typeof personName !== "string") return "Error: Invalid data type";
  
//   const [firstName, middleName, lastName] = personName.split(" ");
  
//   if (!firstName) return "Error: Please enter a name";

//   return {
//     firstName,
//     middleName: middleName || null,
//     lastName: lastName || null,
//   };
// }

// console.log(getSplitName("Adi Daniela Pranata"));
// console.log(getSplitName("Dwi  Kuncoro"));
// console.log(getSplitName("Aurora"));
// console.log(getSplitName(0));
// console.log(getSplitName(""));

// ==================== 🔥 SOAL NO 6 🔥 ===============================
function getAngkaTerbesarKedua(dataNumbers) {
  // check
  if (!Array.isArray(dataNumbers) || dataNumbers.length < 2) {
    return "Error: Invalid data type or array length should be 2 or longer";
  }
// mengurutkan array
  const sortedNumbers = dataNumbers.sort((a, b) => b - a);
  const secondLargest = sortedNumbers[1];
  return secondLargest;
  
}

const dataNumbers = [9, 4, 7, 7, 4, 3, 2, 2, 8];
const dataNumbers3 = [2,4,5,6,99,45,33,88,7,8,6,5,45,34,34,2];
const dataNumbers2 = [9, 4];
console.log(getAngkaTerbesarKedua(dataNumbers)); // Output: 8
console.log(getAngkaTerbesarKedua(dataNumbers2)); // Output: 4
console.log(getAngkaTerbesarKedua(dataNumbers3)); // Output: 34
console.log(getAngkaTerbesarKedua(0)); // Output: Error: Invalid data type or array length should be 2 or longer
console.log(getAngkaTerbesarKedua()); // Output: Error: Invalid data type or array length should be 2 or longer


// ==================== 🔥 SOAL NO 7 🔥 ===============================

// const dataPenjualanPakAldi = [
//   {
//     namaProduct: "Sepatu Futsal Nike Vapor Academy 8",
//     hargaSatuan: 760000,
//     kategori: "Sepatu Sport",
//     totalTerjual: 90,
//   },
//   {
//     namaProduct: "Sepatu Warrior Tristan Black Brown High",
//     hargaSatuan: 960000,
//     kategori: "Sepatu Sneaker",
//     totalTerjual: 37,
//   },
//   {
//     namaProduct: "Sepatu Warrior Tristan Maroon High ",
//     kategori: "Sepatu Sneaker",
//     hargaSatuan: 360000,
//     totalTerjual: 90,
//   },
//   {
//     namaProduct: "Sepatu Warrior Rainbow Tosca Corduroy",
//     hargaSatuan: 120000,
//     kategori: "Sepatu Sneaker",
//     totalTerjual: 90,
//   },
// ];

// const getTotalPenjualan = (dataPenjualan) => {
//   if (!Array.isArray(dataPenjualan)) return "Error: Data Penjualan harus berupa Array";
//   const totalPenjualan = dataPenjualan.reduce((total, data) => total + data.totalTerjual, 0);
//   return isNaN(totalPenjualan) ? "Error: Format data salah" : totalPenjualan;
// };

// console.log(getTotalPenjualan(dataPenjualanPakAldi));

// ==================== 🔥 SOAL NO 8 🔥 ===============================

  

// function getInfoPenjualan(dataPenjualan) {
//   if (!Array.isArray(dataPenjualan)) {
//     throw new Error('Parameter harus bertipe array');
//   }
// deklarasi
//   let totalKeuntungan = 0;
//   let totalModal = 0;
//   let produkBukuTerlaris = '';
//   let jumlahTerjualTerbanyak = 0;
//   let penulisTerlaris = [];
//   let penulisJumlahTerjual = 0;

//   for (let i = 0; i < dataPenjualan.length; i++) {
//     const { hargaBeli, hargaJual, totalTerjual, penulis } = dataPenjualan[i];

//     totalModal += hargaBeli * totalTerjual;
//     totalKeuntungan += hargaJual * totalTerjual - hargaBeli * totalTerjual;

//     if (totalTerjual > jumlahTerjualTerbanyak) {
//       jumlahTerjualTerbanyak = totalTerjual;
//       produkBukuTerlaris = dataPenjualan[i].namaProduk;
//     }

//     if (!penulisTerlaris[penulis]) {
//       penulisTerlaris[penulis] = 0;
//     }
//     penulisTerlaris[penulis] += totalTerjual;

//     if (penulisTerlaris[penulis] > penulisJumlahTerjual) {
//       penulisJumlahTerjual = penulisTerlaris[penulis];
//       penulisTerlaris = penulis;
//     }
//   }

//   const persentaseKeuntungan = (totalKeuntungan / totalModal) * 100;

//   return {
//     totalKeuntungan: `Rp ${totalKeuntungan.toLocaleString('id-ID')},00`,
//     totalModal: `Rp ${totalModal.toLocaleString('id-ID')},00`,
//     persentaseKeuntungan: `${persentaseKeuntungan.toFixed(0)}%`,
//     produkBukuTerlaris,
//     penulisTerlaris,
//   };
// }

// const dataPenjualanNovel = [
//   {
//     idProduct: 'BOOK002421',
//     namaProduk: 'Pulang - Pergi',
//     penulis: 'Tere Liye',
//     hargaBeli: 60000,
//     hargaJual: 86000,
//     totalTerjual: 150,
//     sisaStok: 17,
//   },
//   {
//     idProduct: 'BOOK002351',
//     namaProduk: 'Selamat Tinggal',
//     penulis: 'Tere Liye',
//     hargaBeli: 75000,
//     hargaJual: 103000,
//     totalTerjual: 171,
//     sisaStok: 20,
//   },
//   {
//     idProduct: 'BOOK002941',
//     namaProduk: 'Garis Waktu',
//     penulis: 'Fiersa Besari',
//     hargaBeli: 67000,
//     hargaJual: 99000,
//     totalTerjual: 213,
//     sisaStok: 5,
//   },
//   {
//     idProduct: 'BOOK002941',
//     namaProduk: 'Laskar Pelangi',
//     penulis: 'Andrea Hirata',
//     hargaBeli: 55000,
//     hargaJual: 68000,
//     totalTerjual: 20,
//     sisaStok: 56,
//   },
// ];

// const infoPenjualanNovel = getInfoPenjualan(dataPenjualanNovel);

// console.log(infoPenjualanNovel);


